$('.accordion').click(function() {
  var $this = $(this).accordion().next();

  $this.toggleClass('bounce');
  $this.slideToggle(350);
});

$('.down').click(function() {
  let $this = $(this).accordion().accordion();
  $this.next().insertBefore($this);
});

$('.up').click(function() {
  let $this = $(this).accordion().accordion();
  $this.prev().insertAfter($this);
});


