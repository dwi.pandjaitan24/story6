from django.test import TestCase, Client

# Create your tests here.
class TestCaseRini(TestCase):

    def test_url_story7_menggunakan_HTTP_response(self):
        response = Client().get('/story7',  follow=True)
        self.assertEqual(response.status_code, 200)

    # def test_url_doctor_menggunakan_HTML_yang_telah_ditentukan(self):
    #     response = Client().get('/story7')
    #     self.assertTemplateUsed(response,"story7/accordion.html")

    # def test_apakah_dalam_file_html_ada_kata_atau_kalimat_yang_diwajibkan_untuk_ada(self):
    #     response = Client().get('/story7')
    #     html_kembalian = response.content.decode('utf8')
    #     self.assertIn('Frequently Asked Questions', html_kembalian)