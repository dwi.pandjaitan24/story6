from django.http import request
from django.shortcuts import render

def accordion(request):
     return render(request, 'story7/accordion.html')