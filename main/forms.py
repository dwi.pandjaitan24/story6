from django.db.models import fields
from django.forms import ModelForm
from .models import Peserta, Kegiatan


class FormPeserta(ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'nama_peserta', 
            'kegiatan'
            ]

class FormKegiatan(ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'nama_kegiatan',
            ]

