from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=200)
    
    def __str__(self):
        return self.nama_kegiatan


class Peserta(models.Model):
    nama_peserta = models.CharField(max_length=100)
    kegiatan = models.ForeignKey('Kegiatan', on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nama_peserta
