from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('kegiatan/', views.kegiatan, name='kegiatan'),
    path('peserta/', views.peserta, name='peserta'),
]
