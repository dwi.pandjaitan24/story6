from django.http import request
from django.shortcuts import redirect, render
from .forms import FormPeserta, FormKegiatan
from .models import Peserta, Kegiatan


def home(request):
    anggota = Peserta.objects.all()
    event = Kegiatan.objects.all()
    context = {'peserta': anggota, 'kegiatan': event}
    return render(request, 'main/home.html', context)


def kegiatan(request):
    form = FormPeserta(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('main:home')
    context = {'form':form}
    return render(request, "main/kegiatan.html", context)

def peserta(request):
    form = FormKegiatan(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('main:home')
    context = {'form':form}
    return render(request, "main/peserta.html", context)
