from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .models import Kegiatan, Peserta


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

#yang atas punya kak Sage, ini punya Rini wkwk
class TestCaseRini(TestCase):

    def test_make_model(self):
        kegiatan_obj = Kegiatan.objects.create(
            nama_kegiatan="Aktifitas 1"
        )
        Peserta.objects.create(
            nama_peserta="peserta 1",
            kegiatan=kegiatan_obj
        )

    def test_root_url_check(self):
        response = Client().get('/',  follow=True)
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_url_check(self):
        response = Client().get('/kegiatan/',  follow=True)
        self.assertEqual(response.status_code, 200)

    def test_peserta_url_check(self):
        response = Client().get('/peserta', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_views_check(self):
        response = Client().post("/kegiatan/",  follow=True, form={
            'nama_peserta': 'peserta 1',
            'kegiatan': 'Aktifitas 1'
        })
        self.assertEqual(response.status_code, 200)

    def test_model_check(self):
        kegiatan_obj = Kegiatan.objects.create(nama_kegiatan="Aktifitas 1")
        peserta_obj = Peserta.objects.create(nama_peserta="peserta 1",kegiatan=kegiatan_obj)

        kegiatan_obj = Kegiatan.objects.get(nama_kegiatan="Aktifitas 1")
        self.assertEqual(kegiatan_obj.nama_kegiatan, "Aktifitas 1")

        peserta_obj = Peserta.objects.get(nama_peserta="peserta 1")
        self.assertEqual(peserta_obj.nama_peserta, "peserta 1")
        self.assertEqual(peserta_obj.kegiatan, kegiatan_obj)

